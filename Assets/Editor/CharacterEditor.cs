﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CharacterManager))]
public class CharacterEditor : Editor {

	public override void OnInspectorGUI()
	{
		CharacterManager manager = (CharacterManager)target;
		GUI.skin.label.fontSize = 20;
		GUILayout.Label ("This is a Label in a Custom Editor");
		base.OnInspectorGUI ();
	}

}
