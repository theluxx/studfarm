﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NarrativeEvent))]
public class NarrativeEventEditor : Editor {

	public override void OnInspectorGUI()
	{
		NarrativeEvent manager = (NarrativeEvent)target;
		base.OnInspectorGUI ();
	}
}
