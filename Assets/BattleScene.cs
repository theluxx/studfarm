﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class BattleScene : MonoBehaviour {


	[SerializeField]
	private Animator fadeAnimator;
	[SerializeField]
	private Animator blackAnimator;
	[SerializeField]
	private Animator bullAnimator;
	[SerializeField]
	private TextMeshProUGUI countDownText;

	[SerializeField]
	private Button mashButton;

	[SerializeField]
	private ScenarioManager manager;

	[SerializeField]
	private GameObject battleEventSystem;

	[SerializeField]
	private AudioSource audioPlayer;

	[SerializeField]
	public PlayerMeter player;



	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnEnable()
	{
		manager = FindObjectOfType<ScenarioManager> ();
		fadeAnimator.gameObject.SetActive (true);
		fadeAnimator.SetTrigger ("FadeIn");
		Invoke ("DisableFader", 1.1f);

	}

	public void DisableFader ()
	{
		fadeAnimator.gameObject.SetActive (false);
		blackAnimator.SetTrigger ("BlackOut");
		bullAnimator.SetTrigger ("Begin");
		StartCoroutine ("CountDown");
	}

	public void EnableFader()
	{
		fadeAnimator.gameObject.SetActive (true);
		fadeAnimator.SetTrigger ("FadeOut");
	}

	public IEnumerator CountDown()
	{
		countDownText.gameObject.SetActive (true);
		int timer = 3;

		while (timer > 0)
		{
			countDownText.text = timer.ToString ();
			timer--;
			yield return new WaitForSeconds(1);
		}
		countDownText.text = player.ReturnAction();
		mashButton.interactable = true;
		audioPlayer.Play ();
		yield return new WaitForSeconds (1f);
		countDownText.gameObject.SetActive (false);

	}


	public void LoadDialogueScene()
	{
		StartCoroutine ("DialogueSceneLoad");
	}

	public IEnumerator DialogueSceneLoad()
	{
		EnableFader ();
		yield return new WaitForSeconds (1.1f);
		battleEventSystem.SetActive (false);
		manager.LoadDialogueScene ();

		Destroy (this.gameObject);

	}

	public void FadeMusic()
	{
		StartCoroutine ("MusicOut");
	}



	public IEnumerator MusicOut()
	{
		float percent = 1;
		while(percent > 0)
		{
			percent -= (float)(Time.deltaTime / 2);
			audioPlayer.volume = percent;
			yield return null;
		}
		audioPlayer.Stop ();
		audioPlayer.volume = 1;
	}
}
