﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoiceButton : MonoBehaviour {

	[SerializeField]
	private ScenarioManager manager;


	public void MakeSelection()
	{
		Debug.Log ("This button was " + this.gameObject.transform.GetSiblingIndex().ToString());
		Debug.Log ("This button was " + this.gameObject.name);
		manager.MakeChoice (this.gameObject.transform.GetSiblingIndex ());
	}

}
