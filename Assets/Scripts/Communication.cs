﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[System.Serializable]
public class Communication {


//	[SerializeField]
//	private string name;
//
	public enum Name {Jack = 0, Flora, Clarabell, Buttercup, McDonald};
	public enum Emotion {Neutral = 0, Serious, Angry, Sad, Thinking, Pain, Happy, Surprise, Guilty, Phone, Narrator};
	public enum Scenario {Story = 0, Dialogue, Video, Choice};
	public enum Location {Home = 0, Cafe, PC, Bedroom, Phone, Custom, Emotion};
	public enum DialogBoxEntry {left = 0, right, story};
	public Texture2D customAvatar;

	public Name nameOfCharacter;
	public Emotion emotion;
	public Scenario scenario;
	public Location location;
	public Texture customImage;
	public VideoClip movieClip;
	public AudioClip audioClip;


	[SerializeField]
	private DialogBoxEntry dialogBoxEntry;

	[SerializeField]
	[TextArea(3, 10)]
	private string[] sentences;

	[SerializeField]
	[TextArea(2,10)]
	private string[] choices;

//	public string ReturnName()
//	{
//		return name;
//	}
//
	public string[] Sentences()
	{
		return sentences;
	}

	public DialogBoxEntry Entry()
	{
		return dialogBoxEntry;
	}

	public string ReturnChoices(int choice)
	{
		return choices [choice];
	}

	public int ReturnChoiceCount()
	{
		return choices.Length;
	}
}
