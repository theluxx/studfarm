﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrativeEvent : MonoBehaviour {

	private string chapterName;
	[SerializeField]
	public List<Communication> conversation = new List<Communication>();

	public string ReturnChapterName()
	{
		return chapterName;
	}

	public void Awake()
	{
		chapterName = this.gameObject.name;
	}

}
