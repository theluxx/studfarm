﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class ScenarioManager : MonoBehaviour {

	[SerializeField]
	private Queue<string> sentences;


	[SerializeField]
	public GameObject dialogueScene;
	[SerializeField]
	private Animator dialogueFader;

	[SerializeField]
	private TextMeshProUGUI nameTextLeft;
	[SerializeField]
	private TextMeshProUGUI dialogueTextLeft;
	[SerializeField]
	private TextMeshProUGUI storyText;


	[SerializeField]
	private GameObject pauseMenu;

	[SerializeField]
	private AudioSource audioPlayer;
	[SerializeField]
	public BackgroundAudio bgAudioPlayer;

	[SerializeField]
	private string currentChapter;

	[SerializeField]
	private TextMeshProUGUI nameTextRight;
	[SerializeField]
	private TextMeshProUGUI dialogueTextRight;
	[SerializeField]
	private Animator dialogueAnimatorLeft;
	[SerializeField]
	private Animator dialogueAnimatorRight;

	[SerializeField]
	private GameObject storyNextButton;
	[SerializeField]
	private GameObject fwdButton;
	[SerializeField]
	private float textRevealSpeed;



	[SerializeField]
	List<Communication> conversation = new List<Communication>();

	private int currentLocation;

	[SerializeField]
	private RawImage avatarLeft;

	[SerializeField]
	private RawImage avatarRight;

	[SerializeField]
	private Texture2D[] backgroundTextures;
	[SerializeField]
	private VideoPlayer videoPlayer;
	[SerializeField]
	private VideoClip emotionClouds;
	[SerializeField]
	private Color[] emotionColours;
	[SerializeField]
	private RawImage backgroundImage;
	[SerializeField]
	private Animator backgroundDimmer;
	[SerializeField]
	private Image emotionFilter;
	[SerializeField]
	private GameObject phoneBackground;

	[SerializeField]
	private GameObject faceSpaceLogo;
	[SerializeField]
	private RawImage dialogBoxLeftImage;
	[SerializeField]
	private RawImage dialogBoxRightImage;

	[SerializeField]
	private Texture defaultTextBox;
	[SerializeField]
	private Texture faceSpaceTextBox;

	[SerializeField]
	private Texture blankImage;

	/// <summary>
	/// DEBUG BUTTON
	/// </summary>
	[SerializeField]
	private GameObject testButton;

	[SerializeField]
	private GameObject twoChoiceButtons;
	[SerializeField]
	private TextMeshProUGUI[] doubleChoices;
	[SerializeField]
	private GameObject threeChoiceButtons;
	[SerializeField]
	private TextMeshProUGUI[] tripleChoices;

	[SerializeField]
	private List<NarrativeEvent> narrativeEvent = new List<NarrativeEvent> ();
	[SerializeField]
	private int narrativeEventIndex;

	[SerializeField]
	private Animator narratorStateMachine;
	[SerializeField]
	private Animator narrativeTreeFSM;
	[SerializeField]
	private int playerBias;
	[SerializeField]
	private bool positiveAI;


	[SerializeField]
	public int hero;
	[SerializeField]
	public int opponent;
	[SerializeField]
	public string action;

	int conversationIndex;

	[SerializeField]
	private CharacterManager characterManager;

	public void Awake()
	{
		DontDestroyOnLoad (this.gameObject);
	}

	// Use this for initialization
	public void Start () {

		currentLocation = 0;
		playerBias = 7;
		int x = Random.Range (0, 2);
		Debug.Log ("X is " + x.ToString ());
		//positiveAI = (x == 0) ? true : false;
		Debug.Log ("AI is set to " + positiveAI.ToString ());

		InitializeIndexes ();
		audioPlayer = GetComponent<AudioSource> ();
		narratorStateMachine = this.gameObject.GetComponent<Animator> ();
		characterManager = this.gameObject.GetComponent<CharacterManager> ();
		sentences = new Queue<string> ();

		StartDialogue (conversation[conversationIndex]);


	}

	public void Update()
	{
		
		EscapePause ();

	}


	public void InitializeIndexes()
	{
		conversationIndex = 0;
		narrativeEventIndex = 0;
		currentChapter = narrativeEvent [narrativeEventIndex].ReturnChapterName ();
		Debug.Log ("Current chapter is " + currentChapter);
		narrativeTreeFSM.SetInteger ("Choice", -1);
		conversation = narrativeEvent[narrativeEventIndex].conversation;
		sentences = new Queue<string> ();
	}

	public void TestDialogue()
	{
		if(conversationIndex != conversation.Count)
		{
			StartDialogue (conversation [conversationIndex]);
		}
		else
		{
			Debug.Log ("All conversations finished");
		}
		testButton.SetActive (false);

	}


	public void StartDialogue(Communication dialogue)
	{


		dialogue = conversation [conversationIndex];
		narratorStateMachine.SetInteger ("Scenario", (int)dialogue.scenario);
		narratorStateMachine.SetTrigger ("StartScenario");

		sentences.Clear ();
		foreach(string sentence in dialogue.Sentences())
		{
			sentences.Enqueue (sentence);
		}
		if(currentLocation != (int)dialogue.location || (backgroundImage.texture != dialogue.customImage && dialogue.location == Communication.Location.Custom))
		{
			currentLocation = (int)dialogue.location;
			StartCoroutine ("FadeToLocation", dialogue);


			if(dialogue.location != Communication.Location.Custom && bgAudioPlayer.audioPlayer.clip != dialogue.audioClip)
			{
				//bgAudioPlayer.StopBackgroundSound ();
				Debug.Log ("I am being called");
			}
			if(dialogue.location == Communication.Location.Custom)
			{
				if(dialogue.audioClip != null)
				{
				//	bgAudioPlayer.PlayBackgroundAudio (dialogue.audioClip);
					Debug.Log ("I am being falled, clip name is " + dialogue.audioClip.ToString() );
				}

			}
			else
			{
			//	bgAudioPlayer.PlayBackgroundAudio ((int)dialogue.location);
				Debug.Log ("I am being talled");
			}

			Debug.Log ("I should play appropriate sound");
			Invoke ("DisplayNext", 2.5f);
		}
		else
		{
			
			currentLocation = (int)dialogue.location;
			DisplayNext ();
		}

	}


	public void DisplayNext()
	{
		if(narrativeEventIndex == narrativeEvent.Count)
		{
			return;
		}
		storyText.text = "";
		dialogueTextLeft.text = "";
		dialogueTextRight.text = "";
		storyNextButton.SetActive(false);
		Communication dialogue = conversation [conversationIndex];
		if(dialogue.Entry() == Communication.DialogBoxEntry.left)
		{
			InitializeStringInvisible (dialogueTextLeft.gameObject);
			dialogueAnimatorLeft.SetBool ("IsOpen", true);
			nameTextLeft.text = dialogue.nameOfCharacter.ToString();
			if(dialogue.customAvatar != null)
			{
				avatarLeft.texture = dialogue.customAvatar;
			}
			else
			{
				avatarLeft.texture = characterManager.ReturnAvatar(dialogue.nameOfCharacter.ToString(), dialogue.emotion);
			}

			if(avatarLeft.texture == null)
			{
				nameTextLeft.text = "";
				avatarLeft.texture = blankImage;

			}
		}
		else if(dialogue.Entry() == Communication.DialogBoxEntry.right)
		{
			InitializeStringInvisible (dialogueTextRight.gameObject);
			dialogueAnimatorRight.SetBool ("IsOpen", true);
			nameTextRight.text = dialogue.nameOfCharacter.ToString();
			if(dialogue.customAvatar != null)
			{
				avatarRight.texture = dialogue.customAvatar;
			}
			else
			{
				avatarRight.texture = characterManager.ReturnAvatar(dialogue.nameOfCharacter.ToString(), dialogue.emotion);
			}
			if(avatarRight.texture == null)
			{
				nameTextRight.text = "";
				avatarRight.texture = blankImage;
			}
		}


		if(sentences.Count == 0)
		{
			InitializeStringInvisible (storyText.gameObject);
			StartCoroutine ("EndDialogue");
			return;
		}

		string sentence = sentences.Dequeue ();

		StopAllCoroutines ();


		if(dialogue.Entry() == Communication.DialogBoxEntry.left)
		{
			dialogueTextLeft.text = sentence;
			StartCoroutine ("RevealCharacters", dialogueTextLeft.gameObject);
			audioPlayer.PlayOneShot (characterManager.ReturnAudio (dialogue.nameOfCharacter.ToString (), dialogue.emotion));
		}
		else if(dialogue.Entry() == Communication.DialogBoxEntry.right)
		{
			dialogueTextRight.text = sentence;
			StartCoroutine ("RevealCharacters", dialogueTextRight.gameObject);
			audioPlayer.PlayOneShot (characterManager.ReturnAudio (dialogue.nameOfCharacter.ToString (), dialogue.emotion));
		}
		else
		{
			Debug.Log ("I should write story");
			storyText.text = sentence;
			TMP_Text m_textMeshPro = storyText.gameObject.GetComponent<TMP_Text>();
			m_textMeshPro.ForceMeshUpdate();
			m_textMeshPro.maxVisibleCharacters = 0;
			StartCoroutine ("RevealCharacters", storyText.gameObject);
		}

		Debug.Log (sentence);
	}



	public IEnumerator EndDialogue()
	{
		
		Communication dialogue = conversation [conversationIndex];

		if(dialogue.Entry() == Communication.DialogBoxEntry.left)
		{
			dialogueAnimatorLeft.SetBool ("IsOpen", false);
		}
		else if(dialogue.Entry() == Communication.DialogBoxEntry.right)
		{
			dialogueAnimatorRight.SetBool ("IsOpen", false);
		}
		Debug.Log ("End of Conversation");
		yield return new WaitForSeconds (.3f);
		conversationIndex++;
		if(conversationIndex != (conversation.Count))
		{
			StartDialogue (conversation [conversationIndex]);
		}
		else
		{
			narrativeEventIndex++;
			if(narrativeEventIndex != narrativeEvent.Count)
			{
				conversation = narrativeEvent [narrativeEventIndex].conversation;
				conversationIndex = 0;
				StartDialogue (conversation [conversationIndex]);
			}
			else
			{
				Debug.Log ("All conversations ended");
				narrativeTreeFSM.SetTrigger ("Continue");
				narrativeTreeFSM.SetInteger ("PlayerBias", playerBias);
				narrativeTreeFSM.SetBool ("PositiveAI", positiveAI);
			}

		}
	}


	public IEnumerator RevealCharacters(GameObject obj)
	{
		Communication dialogue = conversation [conversationIndex];


		storyNextButton.SetActive(false);
		yield return null;
		// Force and update of the mesh to get valid information.
		TMP_Text m_textMeshPro = obj.GetComponent<TMP_Text>();
		m_textMeshPro.ForceMeshUpdate();
		Debug.Log ("I should be typing stuff");

		int totalVisibleCharacters = m_textMeshPro.textInfo.characterCount; // Get # of Visible Character in text object
		int counter = 0;
		m_textMeshPro.maxVisibleCharacters = 0;
		yield return new WaitForSeconds (.3f);
		if(dialogue.scenario == Communication.Scenario.Video)
		{
			fwdButton.SetActive (false);
		}
		else if(dialogue.scenario == Communication.Scenario.Choice)
		{
			fwdButton.SetActive (false);
			counter = totalVisibleCharacters;
		}
		else
		{
			fwdButton.SetActive (true);
		}

		while ((totalVisibleCharacters + 1) != counter)
		{
			m_textMeshPro.maxVisibleCharacters = counter;
			counter++;

			if(Time.timeScale == 1)
			{
				if(textRevealSpeed == 0)
				{
					yield return null;
				}
				else
				{
					yield return new WaitForSeconds(textRevealSpeed);
				}
			}
			else
			{
				//This is required so coroutines can be paused during global pause state
				yield return new WaitForSeconds (Time.deltaTime);
			}


		}


		fwdButton.SetActive (false);
		yield return null;
		if(dialogue.scenario != Communication.Scenario.Video)
		{
			storyNextButton.SetActive(true);
		}

		Debug.Log("choice is " + dialogue.Entry());

		if(sentences.Count == 0 && dialogue.scenario == Communication.Scenario.Choice)
		{
			StartCoroutine ("DisplayChoices", dialogue.ReturnChoiceCount ());

		}




	//	storyText.gameObject.transform.GetChild (0).gameObject.SetActive (true);
	}

	public void FastForwardButton()
	{
		StopCoroutine ("RevealCharacters");
		Communication dialogue = conversation [conversationIndex];

		fwdButton.SetActive (false);

		if(dialogue.scenario != Communication.Scenario.Video)
		{
			storyNextButton.SetActive(true);
		}

		GameObject obj;

		if(dialogue.Entry() == Communication.DialogBoxEntry.left)
		{
			obj = dialogueTextLeft.gameObject;
		}
		else if (dialogue.Entry() == Communication.DialogBoxEntry.right)
		{
			obj = dialogueTextRight.gameObject;
		}
		else
		{
			obj = storyText.gameObject;
		}

		TMP_Text m_textMeshPro = obj.GetComponent<TMP_Text>();
		m_textMeshPro.ForceMeshUpdate();


		m_textMeshPro.maxVisibleCharacters = m_textMeshPro.textInfo.characterCount;

	}


	public IEnumerator FadeToLocation(Communication dialogue)
	{
		backgroundDimmer.SetTrigger("FadeToLocation");
		yield return new WaitForSeconds(1.1f);
		if(dialogue.location == Communication.Location.Emotion)
		{

			Debug.Log ("Location was emotion");
			StartCoroutine ("PlayEmotionClouds");
		}
		else
		{
			emotionFilter.gameObject.SetActive (false);
			if(dialogue.location == Communication.Location.Phone)
			{
				phoneBackground.SetActive (true);

			}
			else
			{
				phoneBackground.SetActive (false);
				if(dialogue.location == Communication.Location.Custom)
				{
					backgroundImage.texture = dialogue.customImage;
				}
				else
				{
					backgroundImage.texture = backgroundTextures [(int)dialogue.location];
				}


			}
			Debug.Log ("Location was not emotion");


		}


		Debug.Log ("I have finished");
	}


	public void DialogueUI()
	{
		//faceSpaceLogo.SetActive (false);
		dialogBoxLeftImage.texture = defaultTextBox;
		dialogBoxLeftImage.color = Color.blue;
		dialogBoxRightImage.color = Color.blue;
		avatarLeft.gameObject.SetActive (true);
		avatarRight.gameObject.SetActive (true);
		storyText.gameObject.SetActive (false);
	}

	public void StoryUI()
	{
		storyText.gameObject.SetActive (true);
		avatarLeft.gameObject.SetActive (false);
		avatarRight.gameObject.SetActive (false);
	}

	public void ChoiceUI()
	{


		avatarLeft.gameObject.SetActive (true);
		avatarRight.gameObject.SetActive (true);
		storyText.gameObject.SetActive (false);
	}

	public void VideoUI()
	{
		Debug.Log ("I should play video");
		avatarLeft.gameObject.SetActive (false);
		avatarRight.gameObject.SetActive (false);
		storyText.gameObject.SetActive (false);
		StartCoroutine ("PlayVideo", conversation [conversationIndex]);
	}


	private void ActivateStoryNextButton()
	{
		storyText.gameObject.transform.GetChild (0).gameObject.SetActive (true);
		Debug.Log ("Next button will appear");
	}

	private void InitializeStringInvisible(GameObject obj)
	{
		TMP_Text m_textMeshPro = obj.gameObject.GetComponent<TMP_Text>();
		m_textMeshPro.maxVisibleCharacters = 0;
	}

	public IEnumerator DisplayChoices(int choices)
	{
//		leftNextButton.SetActive (false);
//		rightNextButton.SetActive (false);
		storyNextButton.SetActive (false);
		fwdButton.SetActive (false);
		yield return new WaitForSeconds (.5f);

		Communication dialogue = conversation [conversationIndex];
		Debug.Log ("I am a happy man");

		InitializeStringInvisible (storyText.gameObject);

		if(choices == 2)
		{

			twoChoiceButtons.SetActive (true);
			doubleChoices [0].text = dialogue.ReturnChoices (0);
			doubleChoices [1].text = dialogue.ReturnChoices (1);
		}
		else
		{
			threeChoiceButtons.SetActive (true);
			tripleChoices [0].text = dialogue.ReturnChoices (0);
			tripleChoices [1].text = dialogue.ReturnChoices (1);
			tripleChoices [2].text = dialogue.ReturnChoices (2);
		}


	}

	public void MakeChoice(int value)
	{
		Communication dialogue = conversation [conversationIndex];

		if(dialogue.Entry() == Communication.DialogBoxEntry.left)
		{
			dialogueAnimatorLeft.SetBool ("IsOpen", false);
		}
		else if(dialogue.Entry() == Communication.DialogBoxEntry.right)
		{
			dialogueAnimatorRight.SetBool ("IsOpen", false);
		}
		Debug.Log ("End of Conversation");
		twoChoiceButtons.SetActive (false);
		threeChoiceButtons.SetActive (false);
		narrativeTreeFSM.SetInteger ("Choice", value);
		narrativeTreeFSM.SetTrigger ("Continue");


	}

	public void ReAssignChapter(NarrativeEvent nextEvent)
	{
		narrativeEvent.Clear ();
		narrativeEvent.Add (nextEvent);
		InitializeIndexes ();
		StartDialogue (conversation[conversationIndex]);
	}

	public IEnumerator PlayEmotionClouds()
	{
		bgAudioPlayer.StopBackgroundSound ();
		videoPlayer.clip = emotionClouds;
		videoPlayer.Prepare ();


		WaitForSeconds waitTime = new WaitForSeconds (.5f);

		while(!videoPlayer.isPrepared)
		{
			Debug.Log ("Preparing video");
			yield return waitTime;
			break;
		}

		backgroundImage.texture = videoPlayer.texture;
		videoPlayer.Play ();

		float percent = 0;
		emotionFilter.gameObject.SetActive (true);
		while(percent < 1)
		{
			emotionFilter.color = Color.Lerp (Color.black, emotionColours [playerBias - 1], percent);
			percent += Time.deltaTime;
			yield return null;
		}

	}

	public IEnumerator PlayVideo(Communication dialog)
	{
		bgAudioPlayer.StopBackgroundSound ();
		backgroundDimmer.gameObject.SetActive (true);
		Debug.Log ("I am playing video");
		videoPlayer.isLooping = false;
		videoPlayer.clip = dialog.movieClip;
		videoPlayer.Prepare ();
		storyNextButton.SetActive (false);
		backgroundDimmer.SetTrigger("FadeToLocation");
		yield return new WaitForSeconds (1.1f);
		backgroundImage.gameObject.SetActive (false);
		yield return new WaitForSeconds (1.4f);
		backgroundDimmer.gameObject.SetActive (false);

//		videoPlayer.source = VideoSource.VideoClip;
//
//		videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
//
//		//Assign the Audio from Video to AudioSource to be played
//		videoPlayer.EnableAudioTrack(1, true);
//		videoPlayer.SetTargetAudioSource(1, audioPlayer);
		audioPlayer.clip = dialog.audioClip;
		WaitForSeconds waitTime = new WaitForSeconds (.5f);


		while(!videoPlayer.isPrepared)
		{
			Debug.Log ("Preparing video");
			yield return waitTime;
			break;
		}



		backgroundImage.texture = videoPlayer.texture;

		videoPlayer.Play ();
		audioPlayer.Play ();
		backgroundImage.gameObject.SetActive (true);

		Debug.Log ("AudioClip is " + audioPlayer.clip.ToString ());

		if(dialog.movieClip != null)
		{
			yield return new WaitForSeconds ((float)dialog.movieClip.length);
		}
		videoPlayer.Stop ();
		backgroundDimmer.gameObject.SetActive (true);
		videoPlayer.isLooping = true;
		DisplayNext ();


	}


	public void PauseGame()
	{
		if(dialogueScene.activeInHierarchy == false)
		{
			return;
		}
		Debug.Log ("Game is paused");
		pauseMenu.SetActive (true);
		Time.timeScale = 0;
		if(videoPlayer.isPlaying == true)
		{
			videoPlayer.Pause ();
			audioPlayer.Pause ();
		}
		if(bgAudioPlayer.audioPlayer.isPlaying == true)
		{
			bgAudioPlayer.audioPlayer.Pause ();
		}
	}

	public void ResumeGame()
	{
		Debug.Log ("Game is resumed");
		pauseMenu.SetActive (false);
		Time.timeScale = 1;
		if(videoPlayer.clip != null)
		{
			videoPlayer.Play ();
		}
		if(audioPlayer.clip != null)
		{
			audioPlayer.Play ();
		}
		if(bgAudioPlayer.audioPlayer.clip != null)
		{
			bgAudioPlayer.audioPlayer.Play ();
		}
	}


	public void EscapePause()
	{
		if(pauseMenu.activeInHierarchy == true)
		{
			if(Input.GetKeyDown(KeyCode.Escape))
			{
				Invoke ("ResumeGame", Time.deltaTime);
			}
		}

		if(Input.GetKeyDown(KeyCode.Escape) && pauseMenu.activeInHierarchy == false)
		{
			PauseGame ();
		}
	}



	public void LoadBattleScene()
	{
		

		StartCoroutine ("DisableDialogueScene");

	}


	public IEnumerator DisableDialogueScene()
	{
		dialogueFader.gameObject.SetActive (true);
		dialogueFader.SetTrigger("FadeOut");
		yield return new WaitForSeconds (1.1f);
		SceneManager.LoadScene ("BattleTest");
		dialogueFader.gameObject.SetActive (false);
		Camera.main.GetComponent<AudioListener> ().enabled = false;
		dialogueScene.SetActive (false);


	}


	public void LoadDialogueScene()
	{
		dialogueScene.SetActive (true);
		Camera.main.GetComponent<AudioListener> ().enabled = true;


		StartCoroutine ("EnableDialogueScene");

	}

	public IEnumerator EnableDialogueScene()
	{
		
		dialogueFader.gameObject.SetActive (true);
		dialogueFader.SetTrigger ("FadeIn");
		yield return new WaitForSeconds (1.1f);
		backgroundDimmer.gameObject.GetComponent<Image> ().color = Color.clear;
		dialogueFader.gameObject.SetActive (false);
		narrativeTreeFSM.SetTrigger ("Continue");
		narrativeTreeFSM.SetInteger ("Choice", 1);
		//narrativeTreeFSM.SetInteger ("Choice", 0);
	//	dialogueScene.SetActive (false);

	}

	public void SetupOpponent(int _hero, int _opponent, string _action)
	{

		hero = _hero;
		opponent = _opponent;
		action = _action;
	}



}
