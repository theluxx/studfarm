﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {


	[SerializeField]
	private ScenarioManager manager;
	// Use this for initialization
	void Awake () {
		manager = FindObjectOfType<ScenarioManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ResumeGame()
	{
		manager.ResumeGame ();
	}


	public void ReturnToTitle()
	{
		Time.timeScale = 1;

		SceneManager.LoadScene ("StartScreen");
		Destroy (manager.gameObject);
		Destroy (manager.dialogueScene);
	}

	public void ExitGame()
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit();
		#endif
	}

}
