﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class UIAudioManager : MonoBehaviour {


	[SerializeField]
	private AudioClip confirmationSound;

	[SerializeField]
	private AudioSource audioPlayer;

	// Use this for initialization
	void Start () {

		audioPlayer = GetComponent<AudioSource> ();
	}
	


	public void PlayConfirmSound()
	{
		audioPlayer.PlayOneShot (confirmationSound);
	}

}
