﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

	public Communication dialogue;

	public void TriggerDialogue()
	{
		FindObjectOfType<ScenarioManager> ().StartDialogue (dialogue);
	}

}
