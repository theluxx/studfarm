﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrativeTree : MonoBehaviour {

	[SerializeField]
	public ScenarioManager manager;

	[SerializeField]
	public NarrativeEvent chapterA1, chapterA2, chapterA3, chapterA4, chapterA5, chapterA6, chapterA7;

	public NarrativeEvent chapterA8, chapterA9, chapterA10, chapterA11, chapterA12, chapterA13, chapterA14;

	public NarrativeEvent chapterA15, chapterA16, chapterA17, chapterA18, chapterA19;

	public NarrativeEvent chapterB1, chapterB2, chapterB3, chapterB4, chapterB5, chapterB6, chapterB7;

	public NarrativeEvent chapterB8, chapterB9, chapterB10, chapterB11, chapterB12, chapterB13, chapterB14;

	public NarrativeEvent chapterB15, chapterB16, chapterB17, chapterB18, chapterB19, chapterB20, chapterB21;

	public NarrativeEvent chapterB22, chapterB23, chapterB24;

	public NarrativeEvent chapterC1, chapterC2, chapterC3, chapterC4, chapterC5, chapterC6, chapterC7;
	public NarrativeEvent chapterC8, chapterC9, chapterC10, chapterC11, chapterC12, chapterC13, chapterC14;
	public NarrativeEvent chapterC15, chapterC16, chapterC17, chapterC18, chapterC19, chapterC20, chapterC21;
	public NarrativeEvent chapterC22, chapterC23, chapterC24, chapterC25, chapterC26, chapterC27, chapterC28;
	public NarrativeEvent chapterC29, chapterC30, chapterC31, chapterC32, chapterC33, chapterC34, chapterC35;
}
