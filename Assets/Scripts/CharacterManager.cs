﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterManager : MonoBehaviour {

	private List<Texture2D> leftAvatars = new List<Texture2D>();
	private List<AudioClip> ethanAudio = new List<AudioClip> ();
	private List<Texture2D> rightAvatars = new List<Texture2D>();
	private List<AudioClip> davidAudio = new List<AudioClip> ();

	/// <summary>
	/// Ethan's Avatars
	/// </summary>
	[Header("Ethan's Avatars")]
	[SerializeField]
	private Texture2D ethanNeutralTex;
	[SerializeField]
	private Texture2D ethanSeriousTex;
	[SerializeField]
	private Texture2D ethanAngryTex;
	[SerializeField]
	private Texture2D ethanSadTex;
	[SerializeField]
	private Texture2D ethanThinkingTex;
	[SerializeField]
	private Texture2D ethanPainTex;
	[SerializeField]
	private Texture2D ethanHappyTex;
	[SerializeField]
	private Texture2D ethanSurpriseTex;
	[SerializeField]
	private Texture2D ethanGuiltyTex;
	[SerializeField]
	private Texture2D ethanPhoneTex;
	[SerializeField]
	private Texture2D ethanNarratorTex;

	[Header("Ethan's Audio")]
	[SerializeField]
	private AudioClip ethanNeutralClip;
	[SerializeField]
	private AudioClip ethanSeriousClip;
	[SerializeField]
	private AudioClip ethanAngryClip;
	[SerializeField]
	private AudioClip ethanSadClip;
	[SerializeField]
	private AudioClip ethanThinkingClip;
	[SerializeField]
	private AudioClip ethanPainClip;
	[SerializeField]
	private AudioClip ethanHappyClip;
	[SerializeField]
	private AudioClip ethanSurpriseClip;
	[SerializeField]
	private AudioClip ethanGuiltyClip;
	[SerializeField]
	private AudioClip ethanPhoneClip;
	[SerializeField]
	private AudioClip ethanNarratorClip;

	[Header("David's Avatars")]
	[SerializeField]
	private Texture2D davidNeutralTex;
	[SerializeField]
	private Texture2D davidSeriousTex;
	[SerializeField]
	private Texture2D davidAngryTex;
	[SerializeField]
	private Texture2D davidSadTex;
	[SerializeField]
	private Texture2D davidThinkingTex;
	[SerializeField]
	private Texture2D davidPainTex;
	[SerializeField]
	private Texture2D davidHappyTex;
	[SerializeField]
	private Texture2D davidSurpriseTex;
	[SerializeField]
	private Texture2D davidGuiltyTex;
	[SerializeField]
	private Texture2D davidPhoneTex;
	[SerializeField]
	private Texture2D davidNarratorTex;

	[Header("David's Audio")]
	[SerializeField]
	private AudioClip davidNeutralClip;
	[SerializeField]
	private AudioClip davidSeriousClip;
	[SerializeField]
	private AudioClip davidAngryClip;
	[SerializeField]
	private AudioClip davidSadClip;
	[SerializeField]
	private AudioClip davidThinkingClip;
	[SerializeField]
	private AudioClip davidPainClip;
	[SerializeField]
	private AudioClip davidHappyClip;
	[SerializeField]
	private AudioClip davidSurpriseClip;
	[SerializeField]
	private AudioClip davidGuiltyClip;
	[SerializeField]
	private AudioClip davidPhoneClip;
	[SerializeField]
	private AudioClip davidNarratorClip;

	public void Awake()
	{
		InitializeEthanAvatars ();
		InitializeEthanAudio ();
		InitializeDavidAvatars ();
		InitializeDavidAudio ();
	}

	public Texture2D ReturnAvatar(string name, Communication.Emotion emotion)
	{

		switch (name) {
		case "Jack":
			return leftAvatars [(int)emotion];
		case "Flora":
			return rightAvatars [(int)emotion];
		default:
			Debug.LogError ("Should not have reached here, no such character name exists");
			return leftAvatars [0];


		}
	}

	public AudioClip ReturnAudio(string name, Communication.Emotion emotion)
	{

		switch (name) {
		case "Jack":
			return ethanAudio [(int)emotion];
		case "Flora":
			return davidAudio [(int)emotion];
		default:
			Debug.LogError ("Should not have reached here, no such character name exists");
			return ethanAudio [0];


		}
	}


	public void InitializeEthanAvatars()
	{
		leftAvatars.Add (ethanNeutralTex);
		leftAvatars.Add (ethanSeriousTex);
		leftAvatars.Add (ethanAngryTex);
		leftAvatars.Add (ethanSadTex);
		leftAvatars.Add (ethanThinkingTex);
		leftAvatars.Add (ethanPainTex);
		leftAvatars.Add (ethanHappyTex);
		leftAvatars.Add (ethanSurpriseTex);
		leftAvatars.Add (ethanGuiltyTex);
		leftAvatars.Add (ethanPhoneTex);
		leftAvatars.Add (ethanNarratorTex);
	}

	public void InitializeEthanAudio()
	{
		ethanAudio.Add (ethanNeutralClip);
		ethanAudio.Add (ethanSeriousClip);
		ethanAudio.Add (ethanAngryClip);
		ethanAudio.Add (ethanSadClip);
		ethanAudio.Add (ethanThinkingClip);
		ethanAudio.Add (ethanPainClip);
		ethanAudio.Add (ethanHappyClip);
		ethanAudio.Add (ethanSurpriseClip);
		ethanAudio.Add (ethanGuiltyClip);
		ethanAudio.Add (ethanPhoneClip);
		ethanAudio.Add (ethanNarratorClip);
	}

	public void InitializeDavidAvatars()
	{
		rightAvatars.Add (davidNeutralTex);
		rightAvatars.Add (davidSeriousTex);
		rightAvatars.Add (davidAngryTex);
		rightAvatars.Add (davidSadTex);
		rightAvatars.Add (davidThinkingTex);
		rightAvatars.Add (davidPainTex);
		rightAvatars.Add (davidHappyTex);
		rightAvatars.Add (davidSurpriseTex);
		rightAvatars.Add (davidGuiltyTex);
		rightAvatars.Add (davidPhoneTex);
		rightAvatars.Add (davidNarratorTex);
	}

	public void InitializeDavidAudio()
	{
		davidAudio.Add (davidNeutralClip);
		davidAudio.Add (davidSeriousClip);
		davidAudio.Add (davidAngryClip);
		davidAudio.Add (davidSadClip);
		davidAudio.Add (davidThinkingClip);
		davidAudio.Add (davidPainClip);
		davidAudio.Add (davidHappyClip);
		davidAudio.Add (davidSurpriseClip);
		davidAudio.Add (davidGuiltyClip);
		davidAudio.Add (davidPhoneClip);
		davidAudio.Add (davidNarratorClip);
	}
}
