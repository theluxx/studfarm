﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BackgroundAudio : MonoBehaviour {

	[SerializeField]
	private List<AudioClip> backgroundSound = new List<AudioClip>();
	public AudioSource audioPlayer;
	public float minVolume;
	public float volumeChangeSpeed;

	// Use this for initialization
	void Start () {
		audioPlayer = GetComponent<AudioSource> ();
	}

	public void OnEnable()
	{
		audioPlayer.volume = 1;
		PlayBackgroundAudio (1);
	}

	// Update is called once per frame
	void Update () {

		if(Input.GetKey(KeyCode.K))
		{
			//StartCoroutine ("FadeOutMusic");
		}
	}

	public void PlayBackgroundAudio(int soundNumber)
	{
		audioPlayer.loop = true;
		audioPlayer.clip = backgroundSound [soundNumber];
		audioPlayer.Play ();
	}

	public void PlayBackgroundAudio(AudioClip clip)
	{
		audioPlayer.loop = true;
		audioPlayer.clip = clip;
		audioPlayer.Play ();
	}

	public void StopBackgroundSound()
	{
		audioPlayer.Stop ();
		audioPlayer.loop = false;
		Debug.LogError ("I stopped the sound");
	}

	public IEnumerator FadeOutMusic()
	{
		float percent = 1;
		while(percent > minVolume)
		{
			percent -= (2 * Time.deltaTime);
			audioPlayer.volume -= (2 *(Time.deltaTime));

			Debug.Log ("Volume is " + audioPlayer.volume.ToString ());
			yield return null;
		}
		audioPlayer.volume = minVolume;
		Debug.Log ("Volume is " + audioPlayer.volume.ToString ());

	}

	public IEnumerator FadeInMusic()
	{
		float percent = minVolume;
		while(percent < 1)
		{
			percent += (Time.deltaTime / volumeChangeSpeed);
			audioPlayer.volume += (Time.deltaTime / volumeChangeSpeed);

			Debug.Log ("Volume is " + audioPlayer.volume.ToString ());
			yield return null;
		}
		audioPlayer.volume = 1f;
		Debug.Log ("Volume is " + audioPlayer.volume.ToString ());

	}

	public void FadeMusicIn()
	{
		StartCoroutine ("FadeInMusic");
	}


	public void FadeMusicOut()
	{
		StartCoroutine ("FadeOutMusic");
	}


	public IEnumerator QueueMusic(int song)
	{
		float percent = 1;
		while(percent > 0)
		{
			percent -= (Time.deltaTime / 2);
			audioPlayer.volume -= (Time.deltaTime / 2);

			Debug.Log ("Volume is " + audioPlayer.volume.ToString ());
			yield return null;
		}
		audioPlayer.Stop ();
		audioPlayer.volume = 1;
		PlayBackgroundAudio (song);
		Debug.Log ("I should play a different song");
	}

	public void cueMusic(int song)
	{
		StartCoroutine ("QueueMusic", song);
	}


	public bool IsPlaying()
	{
		return audioPlayer.isPlaying;
	}

}
