﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerMeter : MonoBehaviour 
{
//	public Animator anim;

	[SerializeField]
	private GameObject bullPlacement;
	[SerializeField]
	private GameObject enemyPlacement;
	[SerializeField]
	private AudioSource winMoo;

	[SerializeField]
	private BattleScene battleManager;

	[SerializeField]
	public Texture2D[] opponents;

	[SerializeField]
	public Texture2D[] heroes;


	[SerializeField]
	private GameObject mooText;


	[SerializeField]
	private RawImage leftImage;
	[SerializeField]
	private RawImage rightImage;

	[SerializeField]
	private string _action;

	[Range(0,1)]
	public float fillPerButton = 0.1f;//how much the bar increase per click
	[Range(0,1)]
	public float fillReduce = 0.1f;//how much the bar drains per second

	private bool finish = false; //used to stop the if loop in update()
	private AudioSource[] mooList;
	private float startDistance; //the x position of the bull
	private Image whiteJuice; //Bulljuice gameobject's Image

	// Use this for initialization
	void Start () 
	{
//		anim = GetComponent<Animator> ();
		whiteJuice = GameObject.Find ("BullJuice").GetComponent<Image> ();
		bullPlacement = GameObject.FindGameObjectWithTag("Bull");
		enemyPlacement = GameObject.FindGameObjectWithTag("Enemy");
		winMoo = enemyPlacement.GetComponent<AudioSource> ();
		mooList = GameObject.Find ("MooSounds").GetComponents<AudioSource> ();

		//AssignBattle (heroes [1], opponents [3], "MILK?!?!");

		whiteJuice.fillAmount = 0f; //sets the bar at 0
		startDistance = bullPlacement.transform.position.x; //grabs Bull's x position




	}

	public void OnEnable()
	{
		ScenarioManager currentOpponent = FindObjectOfType<ScenarioManager> ();
		AssignBattle (heroes [currentOpponent.hero], opponents [currentOpponent.opponent], currentOpponent.action);
	}


	// Update is called once per frame
	void Update () {
		if (whiteJuice.fillAmount == 1f && finish == false ) {
			Debug.Log ("you win!");
			winMoo.Play ();
			finish = true;
			mooText.SetActive (true);
			StartCoroutine ("VibrateText");
			battleManager.FadeMusic ();
		} else if (finish == false) {
			whiteJuice.fillAmount -= fillReduce * Time.deltaTime; //decreases bar over time
		//	bullPlacement.transform.position = new Vector3 (whiteJuice.fillAmount * (enemyPlacement.transform.position.x - startDistance) + startDistance, bullPlacement.transform.position.y, bullPlacement.transform.position.z);//moves Bull object back to original position over time.
		}
	}


	public void milkywhitepaste ()
	{
//		anim.SetTrigger ("MilkShake");
		whiteJuice.fillAmount += fillPerButton; //increases fill amount as per fillPerButton's value
//		bullPlacement.transform.position = new Vector3 (whiteJuice.fillAmount * (enemyPlacement.transform.position.x - startDistance) + startDistance, bullPlacement.transform.position.y, bullPlacement.transform.position.z);//increases bulls position along the x axis depending on the fillAmount float.

	}

	public IEnumerator VibrateText()
	{
		float time = 0;

		while(time < 2)
		{
			yield return null;
			time += Time.deltaTime;
			mooText.GetComponent<RectTransform>().position = new Vector3((mooText.GetComponent<RectTransform>().position.x + Random.Range(-1.0f, 1.0f)), (mooText.GetComponent<RectTransform>().position.y + Random.Range(-1.0f, 1.0f)), 0);
		}
		battleManager.LoadDialogueScene ();

	}


	public void AssignBattle(Texture2D hero, Texture2D opponent, string action)
	{
		leftImage.texture = hero;
		rightImage.texture = opponent;
		_action = action;
	}

	public string ReturnAction()
	{
		return _action;
	}

}
